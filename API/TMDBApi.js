const api_token = 'ed6e8aa88d7430d332b9130b9e478c7a';

async function getMoviesFromApiAsync(query) {
  const url = `https://api.themoviedb.org/3/search/movie?api_key=${api_token}&query=${query}`;
  try {
    let response = await fetch(url);
    let json = await response.json();
    return data = json.movies;
  } catch (error) {
    console.error(error);
  }
}

export default getMoviesFromApiAsync;