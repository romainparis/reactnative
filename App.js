import React from 'react';
import { View } from 'react-native';
import Search from './Components/Search';

const App = () => {
  return (
    <View>
      <Search/>
    </View>
  );
};

export default App;