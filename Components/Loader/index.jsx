import React from 'react';
import { View, StyleSheet, Text, Image, ActivityIndicator } from 'react-native';

const Loader = ({color, message}) => {
  return (
    <View style={styles.loader}>
      <ActivityIndicator size="large" color={color || "#0D0D0D"} />
      <Text>
        {message || "Canada Companion App"}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  loader: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    
  }
});

export default Loader;