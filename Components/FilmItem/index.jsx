import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const FilmItem = ({title, content, rating, release_date, cover, vote_count}) => {

  return (
    <View style={styles.film}>
      <View style={styles.cover}>
        <Image style={styles.image} source={{uri:`${cover}`}}/>
      </View>
      <View style={styles.body}>
        <View style={styles.header}>
          <Text style={styles.title}>
            {title}
          </Text>
          {vote_count !== 0 && (
            <Text style={styles.rating}>
              {rating}
            </Text>
          )}
        </View>
        <View style={styles.synopsis}>
          <Text numberOfLines={6}>{content}</Text>
        </View>  
        <View style={styles.release}>
        <Text>{`release date: ${release_date}`}</Text>
      </View> 
      </View>
    </View>
  );
};

const styles = StyleSheet.create ({
  film: {
    margin: 16,
    flexDirection: "row",
    backgroundColor: "#e9e9ef"
  },
  cover: {
    flex: 1
  },
  image: {  
    width: '100%',
    height: 175,
    margin: 8,
    resizeMode: 'stretch',
  },
  body: {
    flex: 2,
    justifyContent: "flex-start",
    alignItems: 'flex-end',
    margin: 8,
  },
  header: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  title: {
    fontSize: 22,
    fontWeight: "bold",
    paddingLeft: 8,
    paddingRight: 16,
    flex: 5
  },
  rating: {
    fontSize: 28,
    color: '#666'
  },
  synopsis: {
    padding: 8
  },
  release: {
    justifyContent: 'flex-end',
    paddingTop: 16,
    fontWeight: "bold"
  }
 
})

export default FilmItem;