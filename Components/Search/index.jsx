import React, {useState, useEffect} from 'react';
import {View, Text, TextInput, FlatList, Button, StyleSheet} from 'react-native';
import FilmItem from '../FilmItem';
import Loader from '../Loader';
import moment from 'moment';

const Search = () => {
  const [loading, setLoading] = useState(true);
  const [query, setQuery] = useState("");
  const [activePage, setActivePage] = useState(0);
  const [data, setData] = useState([]);
  const [resultsCount, setResultsCount] = useState(0);
  const [pagesCount, setPagesCount] = useState(0);
  const [films, setFilms] = useState(null);

  const api_token = 'ed6e8aa88d7430d332b9130b9e478c7a';

  useEffect(()=>{
    if(data && data !== null) {
      setResultsCount(data.total_results);
      setPagesCount(data.total_pages);
      setFilms(data.results);
    }
  }, [data])

  const getFilmsFromApi = () => {
    
  
    // if empty query or last page abort
    if ((query.length < 1 )|| (activePage >= pagesCount) ){
      return
    }
    
    const url = `https://api.themoviedb.org/3/search/movie?api_key=${api_token}&query=${query}&page=${activePage +1}`;

    setActivePage(activePage + 1)

    // init loading animation
    setLoading(true);

    // fetch from api
    return fetch(url)

    // parse response to json
    .then((response) => response.json())


    .then((json) => {
     setData(json);
    })
    .finally(()=>setLoading(false))
    .catch((error) => {
      console.error(error);
    }); 
  }

  const handleCoverPicture = (img) => {
    return `https://image.tmdb.org/t/p/w500${img}`;
  }

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 1500);
  }, []);

  return (
    <View>
      {loading && (
        <Loader />
      )}

      {!loading && (
        <View style={styles.container}>
          <View style={styles.form}>
            <Text>Search</Text>
            <TextInput placeholder={"What are you looking for ?"} 
                      onChangeText={(text)=>setQuery(text)}
                      style={styles.input}
                      onSubmitEditing={()=>getFilmsFromApi(query)}
                      returnKeyType="search"/>
            {resultsCount >= 1 && (
              <Text style={styles.count}>{`${resultsCount} movies found`}</Text>
            )}
            
          </View> 
          <View style={styles.list}>
            <FlatList
              data={films}
              keyExtractor={(item) => item.id.toString()}
              onEndReachedThreshold={.5}
              onEndReached={()=>getFilmsFromApi(query)}
              renderItem={({item}) => 
                <FilmItem 
                  title={item.title}
                  content={item.overview} 
                  rating={item.vote_average}
                  cover={handleCoverPicture(item.poster_path)}
                  release_date={moment(item.release_date).format('DD/MM/yyyy')}
                  vote_count={item.vote_count}
                />
              }
            />
          </View>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
    alignContent: 'flex-start',
    justifyContent: 'center',
    backgroundColor: '#F4F4F4',
  },
  form: {
    flex:1,
    padding: 16,
    paddingTop: 50,
  },
  input: {
    height: 40,
    marginTop: 8,
    marginBottom: 8,
    padding: 10,
    borderColor: "#D0D0D0",
    borderWidth: 1,
    backgroundColor: "#FFFFFF",
  },
  list: {
    flex: 8
  },
  count: {
    fontSize: 16,
    fontWeight: "bold",
    textAlign: 'right',
    paddingTop: 8,
  }
});

export default Search;